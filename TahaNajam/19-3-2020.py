#Sorting Method
S = input()


def s(x):
    if x.islower():
        return ord(x)
    elif x.isupper():
        return ord(x)*100000
    elif x in "13579":
        return ord(x)*10000000000
    else:
        return ord(x)*1000000000000000000

print(*sorted(S, key=s), sep='')

-------------------------------------------------------------
#TriangleQuest
N=int(input())
for i in range(1, N):
    print((10**i)//9*i)

-------------------------------------------------------------
#Captain's Room
K = int(input())
set_S = set()
sumlist_S = 0
for i in input().split():
    I = int(i)
    set_S.add(I)
    sumlist_S += I

print((sum(set_S)*K - sumlist_S)//(K-1))

-------------------------------------------------------------
#CheckSubset
def main():
        for i in range(int(input())):
                a = int(input())
                A = set(input().split())
                b = int(input())
                B = set(input().split())
                print('True' if A.issubset(B) else 'False')

if __name__ == '__main__':
    main()

-------------------------------------------------------------
